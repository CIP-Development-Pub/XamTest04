﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace XamTest04
{
    public partial class Page2 : ContentPage
    {
        public Page2()
        {
            InitializeComponent();
        }

        void OnButtonTab2Clicked(object sender, EventArgs args)
        {
            var ParentPage = this.Parent as TabbedPage;
            ParentPage.CurrentPage = ParentPage.Children[0];
        }
    }
}
